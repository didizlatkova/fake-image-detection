import streamlit as st
import pandas as pd
import os
import json
import joblib
import time

from PIL import Image
from src.models.predictor import Predictor

import tensorflow_hub as hub


@st.cache(show_spinner=False)
def load_test_df() -> pd.DataFrame:
    DATA_PATH = "df_test.csv"
    return pd.read_csv(DATA_PATH).set_index('url_caption')


@st.cache(show_spinner=False)
def load_media_labels():
    with open('../data/external/factuality.json') as f:
        return json.load(f)

@st.cache(show_spinner=False)
def load_model():
    MODEL_PATH = '../src/models/persisted/voting_all.joblib'
    return joblib.load(MODEL_PATH)

@st.cache(show_spinner=False, allow_output_mutation=True)
def load_vectorizer():
    COS_PATH = '../src/models/persisted/cos_sim_vectorizer.joblib'
    return joblib.load(COS_PATH)



st.title('Fauxtography Detector')
st.subheader('Select a claim to fact-check:')

df = load_test_df()
cos_sim_vectorizer = load_vectorizer()
model = load_model()

# Download model from 
# http://storage.googleapis.com/tfhub-modules/google/universal-sentence-encoder-large/3.tar.gz
module_url = "../src/models/persisted/use"
use_embed = hub.Module(module_url)

media_labels = load_media_labels()

idx = st.selectbox('', df.index, format_func=lambda x: df.loc[x].claim)

image = Image.open(f'../data/raw/test/images/{idx}')
st.image(image, caption=df.loc[idx].claim, use_column_width=True)

predictor = Predictor(media_labels, cos_sim_vectorizer, model, use_embed)

if st.button('Fact Check'):
    st.write('This is the result...')
    row = df.loc[idx]
    df_to_predict = pd.DataFrame(
        {'url_caption': row.name, 'claim': row.claim, 'img_main': row.img_main}, index=[0])
    df_result = predictor.predict(df_to_predict)
    st.write(df_result)
