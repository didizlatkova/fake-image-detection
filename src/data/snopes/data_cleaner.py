import pandas as pd
import string
import numpy as np
import os
from dotenv import load_dotenv
load_dotenv()



def is_site_image(url):
    return (url.endswith('gif') or
            'rating-' in url or
            not url.startswith('http') or
            '/dist/' in url or
            'wp-content' in url or
            'images/m/' in url
            )


class DataCleaner:
    def __init__(self):
        img_to_fix_file = '{}/data/helpers/img_to_fix'.format(os.getenv("PROJECT_PATH"))
        with open(img_to_fix_file) as f:
            content = f.readlines()
        self.img_to_remove = [row.strip() for row in content if '=' not in row]
        self.img_to_replace = [(row.strip().split('=')[0], '='.join(row.strip().split('=')[1:])) for row in content if '=' in row]

    def remove_empty_label(self, df: pd.DataFrame) -> None:
        df.label.replace('', np.nan, inplace=True)
        no_label = df[df.label.isnull()]
        print('Removing {} articles with empty label.'.format(len(no_label)))
        df.drop(no_label.index, inplace=True)

    def remove_empty_claim(self, df: pd.DataFrame) -> None:
        df.claim.replace('', np.nan, inplace=True)
        no_claim = df[df.claim.isnull()]
        print('Removing {} articles with empty claim.'.format(len(no_claim)))
        df.drop(no_claim.index, inplace=True)

    def impute_empty_img_main(self, df: pd.DataFrame) -> None:
        df.img_all.fillna('', inplace=True)
        df.img_all = df.img_all.map(
            lambda x: '#'.join([x for x in x.split('#') if not is_site_image(x)]))
        df['img_all_count'] = df.img_all.map(lambda x: len(x.split('#')))
        df.loc[(df.img_main.isnull()) & (df.img_all_count == 1) &
               (df.img_all.notnull()), 'img_main'] = df.img_all

    def remove_empty_img_main(self, df: pd.DataFrame) -> None:
        df.img_main.replace('', np.nan, inplace=True)
        no_img_main = df[df.img_main.isnull()]
        print('Removing {} articles with empty img_main.'.format(len(no_img_main)))
        df.drop(no_img_main.index, inplace=True)

    def norm_label(self, df: pd.DataFrame) -> None:
        df['label'] = df['label'].str.lower()
        table = str.maketrans({key: None for key in string.punctuation})
        df['label'] = df['label'].str.translate(table)
        df['label'] = df['label'].str.strip()

    def merge_labels(self, df: pd.DataFrame) -> None:
        false_labels = ['false', 'mostly false', 'probably false']
        true_labels = ['true', 'mostly true', 'partly true']
        mix_labels = ['mixture', 'miscaptioned', 'unproven', 'undetermined', 
                    'legend', 'outdated', 'multiple — see below', 'multiple', 'not any more']
        
        df.loc[df.label.isin(false_labels), 'label'] = 'false'
        df.loc[df.label.isin(true_labels), 'label'] = 'true'
        df.loc[df.label.isin(mix_labels), 'label'] = 'mixture'
    
    def handle_real_photo_inaccurate_description(self, df: pd.DataFrame) -> None:
        df['label'] = df['label'].map(lambda x: 'miscaptioned' if 'real' in str(
            x) and 'description' in str(x) else x)

    def fix_wrong_images(self, df: pd.DataFrame) -> None:
        print("Fixing wrong images.")
        df.drop(df[df.url_caption.isin(self.img_to_remove)].index, inplace=True)
        for t in self.img_to_replace:
            df.loc[df.url_caption==t[0], 'img_main'] = t[1]

    def use_second_image(self, df: pd.DataFrame) -> None:
        df.loc[df.img_all != '', 'img_main'] = df.img_all.str.split('#').str[0]

    def clean(self, df: pd.DataFrame) -> None:
        self.norm_label(df)
        self.handle_real_photo_inaccurate_description(df)
        self.impute_empty_img_main(df)
        self.remove_empty_img_main(df)
        self.use_second_image(df)
        self.remove_empty_label(df)
        self.remove_empty_claim(df)
        self.fix_wrong_images(df)
        self.merge_labels(df)
