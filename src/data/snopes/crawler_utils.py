import requests
import traceback
from bs4 import BeautifulSoup
import glob
import pandas as pd

def get_page_count():
    try:
        url = 'https://www.snopes.com/fact-check/category/photos/page/2/'
        html = requests.get(url).text
        soup = BeautifulSoup(html, 'html5lib')
        span_text = soup.find('span', 'page-count').text # 'Page 2 of 166'
        return int(span_text.split(' ')[-1])
    except Exception:
        traceback.print_exc()
        print('Unable to get page count!')

def get_hardcoded_page_count():
    return 172

def get_chunks(arr, n):
    """ Yield successive n-sized chunks from arr. """

    for i in range(0, len(arr), n):
        yield arr[i:i + n]

def parse_pages(page_range, stop_at_url=None):
    infos = []
    for page_number in page_range:
            print('Crawling page {}'.format(page_number))
            page_url = 'https://www.snopes.com/fact-check/category/photos/page/{}/'.format(page_number)
            html = requests.get(page_url).text
            soup = BeautifulSoup(html, 'html5lib')
            body = soup.find('main')
            articles = body.find_all('article')
            for article in articles:
                article_url = article.find('a')['href']
                if stop_at_url and article_url == stop_at_url:
                    return infos
                info = parse_article(article_url)
                infos.append(info)
    return infos


def get_claim(soup):
    claim = soup.find('p', 'claim')
    if claim:
        return claim.text

    claim = soup.find('div', 'claim').find('p')
    if claim:
        return claim.text

    claims = [x.text.strip() for x in soup.find_all('p') if x.text.strip().startswith('Claim')]
    if claims:
        return claims[0]

    claims = [x for x in soup.find_all('span') if x.text.strip().startswith('Claim')]
    if claims:
        return str(claims[0].nextSibling)


def get_label(soup):
    label = soup.find('span', 'rating-name')
    if label:
        return label.text

    label = soup.find('a', "claim") or soup.find('div', 'claim-old')
    if label:
        return label.span.text

    label = soup.find('div', 'rating-wrapper').find('h5')
    if label:
        return label.text

    font = soup.find('font', 'status_color') or soup.find('span', 'status_color')
    b = font and font.find('b')
    if b:
        return b.text

    span = soup.find('span', attrs={'style':'white-space: nowrap;'})
    inner_span = span and span.find('span')
    if inner_span:
        return inner_span.text

    font = soup.find('font', attrs={'size': 5}) or soup.find('font', attrs={'size': "+3"})
    strong = font and (font.find('strong') or font.find('b'))
    if strong:
        return strong.text

    b_is = [f for f in soup.find_all('b') if f.find('i')]
    if b_is:
        return b_is[0].text

def parse_article(url):
    html = requests.get(url).text
    soup = BeautifulSoup(html, 'html5lib')

    info = {}
    info['url'] = url
    info['url_caption'] = url.split('/')[-2]
    info['title'] = soup.title.text

    featured_image_div = soup.find("div", "featured-image")
    featured_image = featured_image_div and featured_image_div.find('img')
    info['img_main'] = featured_image and (featured_image.get('data-lazy-src') or featured_image.get('src'))

    main = soup.find('main')
    main_article = main and main.find('article')
    img_all = [x.get('src') for x in main_article.find_all('img') if x.get('src')]
    info['img_all'] = '#'.join(img_all)

    info['claim'] = get_claim(soup)
    info['label'] = get_label(soup)

    claim_has_video = info.get('claim') and 'video' in info['claim'].lower()
    title_has_video = info.get('title') and 'video' in info['title'].lower()
    info['is_video'] = claim_has_video or title_has_video

    author = soup.find('a', 'author')
    info['author'] = author and author.text

    published = soup.find('span', 'date-published')
    info['date_published'] = published and published.text

    updated = soup.find('span', 'date-updated')
    info['date_updated'] = updated and updated.text

    return info

def get_latest_dataset(source_dir: str, prefix: str) -> pd.DataFrame:
    crawls = glob.glob('{}/{}_*.csv'.format(source_dir, prefix))
    latest_dataset = sorted(crawls)[-1]
    return pd.read_csv(latest_dataset, index_col=False)
