import click
import pandas as pd
from typing import Dict, List
from urllib.parse import urlparse
from src.data.snopes import crawler_utils as utils
from src.data.snopes.data_cleaner import DataCleaner
from time import gmtime, strftime
import validators
import json
import logging

MBFC_PATH = 'data/external/MBFC Annotations.xlsx'
MBFC_COLUMN = 'new_merged'


def get_media_factuality() -> Dict[str, str]:
    data_xls = pd.read_excel(MBFC_PATH, MBFC_COLUMN, index_col=None)
    data_xls.drop(data_xls[data_xls.is_factual.isna()].index, inplace=True)
    data_xls.is_factual = data_xls.is_factual.astype(str).str.upper()
    url_dict = dict(zip(data_xls['(NEW) url'].map(lambda x: clean_url(x)), data_xls.is_factual))
    return fix_media_factuality(url_dict)


def fix_media_factuality(url_dict: Dict[str, str]) -> Dict[str, str]:
    url_dict['indiatoday.in'] = url_dict['indiatoday.intoday.in']
    url_dict['uk.businessinsider.com'] = url_dict['businessinsider.com']
    url_dict['businessinsider.com.au'] = url_dict['businessinsider.com']
    url_dict['ftw.usatoday.com'] = url_dict['usatoday.com']
    url_dict['bbc.co.uk'] = url_dict['bbc.com']
    url_dict['ibtimes.co.uk'] = url_dict['ibtimes.com']
    url_dict['gizmodo.com.au'] = url_dict['gizmodo.com']

    url_dict['express.co.uk'] = 'MIXTURE'
    del url_dict['knowyourmeme.com']
    del url_dict['scoopnest.com']

    return url_dict


def clean_url(url):
    return urlparse(url).netloc.replace('www.', '')


def get_label(url: str, media_labels: Dict[str, str]) -> str:
    if not validators.url(url):
        return 'INVALID'
    
    url = clean_url(url)
    if url in media_labels:
        return media_labels[url]
    return 'UNKNOWN'

def get_perc_from_known(media_labels: List[str], desired_label: str):
    known = len(media_labels) - media_labels.count('UNKNOWN') - media_labels.count('INVALID')
    if not known:
        return 0

    n_label = media_labels.count(desired_label)
    return n_label / known

@click.command()
@click.argument('source_dir', type=click.Path(exists=True))
@click.argument('dest_dir', type=click.Path(exists=True))
def get_media_features(source_dir, dest_dir):
    logger = logging.getLogger(__name__)

    media_labels = get_media_factuality()

    with open('{}/factuality.json'.format(dest_dir), 'w') as fp:
        json.dump(media_labels, fp)

    df = utils.get_latest_dataset(source_dir, 'ris')
    df.legal_page_urls = df.legal_page_urls.apply(lambda x: [y.strip(' \'"') for y in str(x)[1:-1].split(',')])
    df['media_labels'] = df.legal_page_urls.apply(lambda x: [get_label(url, media_labels) for url in x])
    df['known_urls'] = df.legal_page_urls.apply(lambda x: [url for url in x if get_label(url, media_labels) != 'UNKNOWN'])

    df['true_perc'] = df.media_labels.apply(lambda x: get_perc_from_known(x, 'TRUE'))
    df['false_perc'] = df.media_labels.apply(lambda x: get_perc_from_known(x, 'FALSE'))
    df['mixed_perc'] = df.media_labels.apply(lambda x: get_perc_from_known(x, 'MIXED'))
    df['invalid_count'] = df.media_labels.apply(lambda x: x.count('INVALID'))
    df['known_count'] = df.media_labels.apply(lambda x: len(x) - x.count('UNKNOWN') - x.count('INVALID'))
    df['total_count'] = df.media_labels.apply(lambda x: len(x))

    del df['media_labels']

    cleaner = DataCleaner()
    cleaner.merge_labels(df)

    now = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    df.to_csv('{}/media_{}.csv'.format(dest_dir, now), index=False)
    logger.info('Result saved to {}'.format('{}/media_{}.csv'.format(dest_dir, now)))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    get_media_features()
