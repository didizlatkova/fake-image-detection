import glob
import logging
import os
from multiprocessing import Pool, cpu_count
from time import gmtime, strftime

import click
import more_itertools as mit
import pandas as pd

import crawler_utils as utils
from data_cleaner import DataCleaner
from img_downloader import ImgDownloader
import tqdm


@click.command()
@click.argument('dest_dir', type=click.Path(exists=True))
@click.argument('only_new', type=bool, default=True)
def crawl(dest_dir, only_new):
    """ Crawls https://www.snopes.com/fact-check/category/photos and writes ouput to dest_dir.
    """

    logger = logging.getLogger(__name__)
    cleaner = DataCleaner()
    downloader = ImgDownloader()


    page_count = utils.get_hardcoded_page_count()
    df_final = pd.DataFrame()
    crawls = glob.glob('{}/crawled_*.csv'.format(dest_dir))
    now = strftime("%Y-%m-%d %H:%M:%S", gmtime())

    if only_new and crawls:
        last_crawl = sorted(crawls)[-1]
        df_last = pd.read_csv(last_crawl, index_col=False)
        last_url_crawled = df_last['url'][0]
        print('Last url crawled is: {}'.format(last_url_crawled))

        infos = utils.parse_pages(page_range=range(1, page_count+1), stop_at_url=last_url_crawled)
        df_new = pd.DataFrame(infos)
        cleaner.clean(df_new)
        logger.info('Found {} new article(s).'.format(len(df_new)))
        if len(df_new) > 0:
            downloader.download(df_new, '{}/images/'.format(dest_dir))
            df_final = pd.concat([df_new, df_last], sort=False)
            df_final.reset_index(drop=True, inplace=True)
    else:
        pages_per_cpu = round(page_count / cpu_count())
        chunks = list(utils.get_chunks(range(1, page_count+1), pages_per_cpu))
        with Pool(cpu_count()) as p:
            page_infos = list(tqdm.tqdm(p.imap(utils.parse_pages, chunks), total=len(chunks)))
        all_infos = list(mit.flatten(page_infos))
        df_final = pd.DataFrame(all_infos)
        cleaner.clean(df_final)
        downloader.download(df_final, '{}/images/'.format(dest_dir))

    if not df_final.empty:
        df_final.to_csv('{}/crawled_{}.csv'.format(dest_dir, now), index=False)

    logger.info('{} articles were crawled in total.'.format(len(df_final)))
    logger.info('Result saved to {}'.format('{}/crawled_{}.csv'.format(dest_dir, now)))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    crawl()
