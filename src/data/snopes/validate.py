import glob
import logging
import os
from multiprocessing import Pool, cpu_count
from time import gmtime, strftime

import click
import more_itertools as mit
import pandas as pd

import crawler_utils as utils


@click.command()
@click.argument('dest_dir', type=click.Path(exists=True))
def validate(dest_dir):
    """ Validates that the latest csv in dest_dir has valid crawled data.
    """

    logger = logging.getLogger(__name__)

    crawls = glob.glob('{}/crawled_*.csv'.format(dest_dir))
    last_crawl = sorted(crawls)[-1]
    df = pd.read_csv(last_crawl, index_col=False)

    assert(all(df['url'].str.startswith('https://www.snopes.com/'))), 'Invalid url found!'
    assert(df['url_caption'].is_unique), 'Duplicated values for url_caption found!'
    assert(all(df['label'].notnull())), 'Empty label found!'
    assert(all(df['claim'].notnull())), 'Empty claim found!'
    assert(all(df['img_main'].notnull())), 'Empty img_main found!'
    assert(not any(df['img_main'].str.contains('lazyload-placeholder'))), 'Images contain placeholders!'
        

    logger.info('{} examples were validated in total.'.format(len(df)))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    validate()
