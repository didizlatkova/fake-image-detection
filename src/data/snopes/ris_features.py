import glob
import logging
import os
from multiprocessing import Pool, cpu_count
from time import gmtime, strftime

import click
import more_itertools as mit
import pandas as pd

from src.data.snopes import crawler_utils as utils
import requests
import json
import tqdm
from retrying import retry, RetryError
from google.protobuf.json_format import MessageToJson
from google.cloud import vision
from google.cloud.vision import types
import io
import shutil
import src
from functools import partial

from dotenv import load_dotenv
load_dotenv()

MATCHERS_ALL = [
    # Fact-checking sites US
    ['factcheck.org'],
    ['politifact'],
    ['snopes'],
    ['reuters'],
    ['truthorfiction'],
    ['climatefeedback'],
    ['gossipcop'],
    # Sites with fact-checking sections US
    ['apnews', 'not-real-news'],
    ['apnews', 'fact-check'],
    ['washingtonpost', 'fact-check'],
    ['azcentral', 'fact-check'],
    ['cbslocal', 'reality-check'],
    ['thenevadaindependent', 'fact-check'],
    ['thegazette', 'fact-check'],
    ['thegazette', 'factchecker'],
    ['nytimes', 'fact-check'],
    ['bridgemi', 'michigan-truth-squad'],
    ['channel3000', 'reality-check'],
    ['kmov', 'fact-check'],
    ['npr', 'fact-check'],
    ['qctimes', 'fact-check'],
    ['politico', 'fact-check'],
    ['weeklystandard', 'fact-check'],
    ['ballotpedia', 'fact_check'],
    ['wral', 'fact-check'],
    ['abcnews', 'fact-check'],
    ['chicagotribune', 'fact-check'],
    ['cnn', 'fact-check'],
    ['theguardian', 'fact-check'],
    ['usatoday', 'fact-check'],
    # UK & other
    ['hoax-slayer'],
    ['fullfact'],
    ['factcheckni'],
    ['theconversation', 'fact-check'],
    ['bbc', 'realitycheck'],
    ['bbc', 'reality-check'],
    ['channel4', 'factcheck'],
    ['theferret', 'fact-check'],
    ['theferret', 'fact-service'],
    ['abc', 'fact-check'],
    ['pbs', 'fact-check'],
    ['foxnews', 'fact-check'],
    # Possibly Arabic
    ['factnameh'],
    ['rouhanimeter'],
    ['thewhistle'],
    ['morsimeter'],
    ['larbitrefact'],
    ['meter.iwatch'],
    ['sebsimeter'],
    ['jomaameter'],
    ['essidmeter'],
    # Special phrases English
    ['debate-highlighted'],
    ['debate-transcript-annotated'],
    ['fact check'],
    ['fact%20check'],
    ['fact+check'],
    ['fact_check'],
    ['fact-check'],
    ['factcheck'],
    ['reality check'],
    ['reality%20check'],
    ['reality+check'],
    ['reality_check'],
    ['reality-check'],
    ['realitycheck'],
    ['fake news'],
    ['fake%20news'],
    ['fake+news'],
    ['fake_news'],
    ['fake-news'],
    ['fakenews']
]


@click.command()
@click.argument('source_dir', type=click.Path(exists=True))
@click.argument('dest_dir', type=click.Path(exists=True))
@click.argument('only_new', type=bool, default=False)
def get_ris_features(source_dir, dest_dir, only_new):
    logger = logging.getLogger(__name__)

    searches = glob.glob('{}/crawled_*.csv'.format(dest_dir))
    df_crawled = utils.get_latest_dataset(source_dir, 'crawled')

    if only_new and searches:
        last_search = sorted(searches)[-1]
        df_last = pd.read_csv(last_search, index_col=False)
        last_url_searched = df_last['img'][0]
        print('Last img searched is: {}'.format(last_url_searched))

        # infos = utils.parse_pages(page_range=range(page_count), stop_at_url=last_url_crawled)
        # df_new = pd.DataFrame(infos)
        # logger.info('Found {} new article(s).'.format(len(df_new)))
        # if len(df_new) > 0:
        #     df_final = pd.concat([df_new, df_last], sort=False)
        #     df_final.reset_index(drop=True, inplace=True)
    else:
        with Pool(cpu_count()) as p:
            max_results = 50
            fn = partial(result_to_features, max_results=max_results)
            infos = list(tqdm.tqdm(p.imap(fn, df_crawled.iterrows()), total=len(df_crawled)))

        df_final = pd.DataFrame(infos)

    df_final.descriptions = df_final.descriptions.astype('object')
    df_final.page_urls = df_final.page_urls.astype('object')
    df_final.legal_page_urls = df_final.legal_page_urls.astype('object')
    df_final.dropna(how='all', inplace=True)

    now = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    if not df_final.empty:
        df_final.to_csv('{}/ris_{}.csv'.format(dest_dir, now), index=False)

    logger.info('{} articles were crawled in total.'.format(len(df_final)))
    logger.info('Result saved to {}'.format('{}/ris_{}.csv'.format(dest_dir, now)))

def result_to_features(t, max_results):
    row = t[1]
    try:
        res = reverse_image_search(row.img_main, row.url_caption, max_results)
    except RetryError:
        print('Could not crawl for {}'.format(row.img_main))
        return {}

    info = {}

    #annotation = res.get('fullTextAnnotation')
    #info['text_on_image'] = annotation and annotation.get('text')
    web = res.get('webDetection')
    pages = web and web.get('pagesWithMatchingImages')
    info['page_urls'] = pages and [x.get('url') for x in pages if x.get('url')]
    info['legal_page_urls'] = info['page_urls'] and [x for x in info['page_urls'] if not is_url_bad(x)]

    web_entities = web and web.get('webEntities')
    info['descriptions'] = web_entities and [x.get('description') for x in web_entities if x.get('description')]

    if 'label' in row:
        info['label'] = row.label

    info['img'] = row.img_main
    info['claim'] = row.claim
    info['url_caption'] = row.url_caption

    return info

def is_url_bad(url):
    if not isinstance(url, str):
        print('Error, the provided parameter is not a string. Cannot determine if it is BAD.')
        return False

    url = url.lower()
    return any([all([item in url for item in row]) for row in MATCHERS_ALL])

def retry_if_error(result):
    if result.get('error'):
        print('Retrying')
        print(result.get('error'))
        return True
    return False

@retry(retry_on_result=retry_if_error, stop_max_attempt_number=5, wait_exponential_multiplier=1000, wait_exponential_max=10000)
def reverse_image_search(img_url, url_caption, max_results):
    image_path = "{}/data/raw/test/images/{}".format(os.getenv("PROJECT_PATH"), url_caption)
    with io.open(image_path, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)
    client = vision.ImageAnnotatorClient()
    ann = client.annotate_image({
        'image': image,
        'features': [
            {'type': vision.enums.Feature.Type.WEB_DETECTION, 'max_results': max_results},
           # {'type': vision.enums.Feature.Type.TEXT_DETECTION, 'max_results': 50}
        ],
    })
    return json.loads(MessageToJson(ann))


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    get_ris_features()
