import pandas as pd
import urllib.request
from multiprocessing import Pool, cpu_count
from functools import partial
import more_itertools as mit
import os

def download_image(t, img_path):
    row = t[1]
    claim = row.url_caption
    img_name = img_path + claim
    img_url = row.img_main
    try:
        urllib.request.urlretrieve(img_url, img_name)
        return []
    except:
        print('Error occured with:')
        print(img_url)
        return [row.url_caption]

class ImgDownloader:
    def download(self, df: pd.DataFrame, dest_dir: str) -> None:
        print("Downloading images.")
        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)

        pool = Pool(cpu_count())
        to_remove = pool.map(partial(download_image, img_path=dest_dir), df.iterrows())
        to_remove = list(mit.flatten(to_remove))
        pool.close()
        pool.join()

        df_to_remove = df[df.url_caption.isin(to_remove)]
        if not df_to_remove.empty:
            print('Removing images: {}'.format(to_remove))
            df.drop(df_to_remove.index, inplace=True)
        print('Downloaded {} images'.format(len(df)))
