from sklearn.feature_extraction.text import TfidfVectorizer

skip_words = ['snopes', 'reuters']
skip_categories = ['unknown', 'uncategorized']

def prefix_analyzer(doc, prefix, lowercase=True, tokenizer=None, ngram_range=(1,1)):
    vectorizer = TfidfVectorizer(stop_words='english', lowercase=lowercase, 
                                 tokenizer=tokenizer, ngram_range=ngram_range)
    word_analyzer = vectorizer.build_analyzer()
    return ['{}_{}'.format(prefix, x) for x in word_analyzer(str(doc)) if x not in 
            skip_words + skip_categories]


def tag_tokenizer(doc):
    return [x.strip(' \'"') for x in str(doc)[1:-1].split(',')]
