from sklearn.base import BaseEstimator, TransformerMixin

class BowSelector(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key]

class ColumnSelector(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        return data_dict[self.key].ravel().reshape(-1,1)
    
    def get_feature_names(self):
        return [self.key]
    
class MultipleSelector(BaseEstimator, TransformerMixin):
    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, data_dict):
        self.feature_names = data_dict.columns[data_dict.columns.str.startswith(self.key)]
        return data_dict[self.feature_names]
    
    def get_feature_names(self):
        return list(self.feature_names)
