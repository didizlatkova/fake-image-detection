import pandas as pd
import numpy as np
import glob

import validators

import more_itertools as mit
import tqdm

import json

from multiprocessing import Pool, cpu_count

from src.data.snopes import ImgDownloader
from src.data.snopes.ris_features import result_to_features
from src.data.snopes.media_features import get_label, get_perc_from_known
from src.features.url_classification import classifier_LICA, classifier_DFR

from urllib.parse import urlparse
import tensorflow as tf
import tensorflow_hub as hub
from joblib import dump, load

from newspaper import Article, ArticleException

from sklearn.metrics.pairwise import cosine_similarity

from functools import partial

import os

from typing import Dict, List
import streamlit as st

def crawl_article(url):
    try:
        article = Article(url)
        article.download()
        article.parse()
        article.nlp()
    except Exception:
        print('Exception on url: {}'.format(url))
        return {}

    return {
        'url': url,
        'title': article.title,
        'text': article.text,
        'authors': article.authors,
        'publish_date': article.publish_date,
        'keywords': article.keywords,
        'summary': article.summary
    }


def clean_url(url):
    return urlparse(url).netloc.replace('www.', '')


def get_title(url, label, df_labels_test):
    if url in df_labels_test.index and df_labels_test.loc[url]['media_label'] == label:
        return str(df_labels_test.loc[url].title)
    return ''


def get_use_similarities(df, claim_embeddings, title_embeddings, df_labels_true):
    urls = df.legal_page_urls
    urls = [y.strip(' \'"') for y in str(urls)[1:-1].split(',')]
    sub_df = df_labels_true[df_labels_true.index.isin(urls)]
    if sub_df.shape[0] == 0:
        return []

    claim_emb = claim_embeddings[df.name]
    title_emb = title_embeddings[sub_df.i]

    return np.inner(claim_emb, title_emb)


class Predictor():
    def __init__(self, media_labels: Dict[str, str], cos_sim_vectorizer, model,
                 use_embed
                 ):
        self.media_labels = media_labels
        self.cos_sim_vectorizer = cos_sim_vectorizer
        self.model = model
        self.use_embed = use_embed

        tf.logging.set_verbosity(tf.logging.ERROR)

    def set_label(self, x):
        if x in self.media_labels:
            return self.media_labels[x]
        return ''

    def get_cos_similarities(self, df, claims, df_labels_true):
        urls = df.legal_page_urls
        urls = [y.strip(' \'"') for y in str(urls)[1:-1].split(',')]
        sub_df = df_labels_true[df_labels_true.index.isin(urls)]
        if sub_df.empty:
            return []

        titles = self.cos_sim_vectorizer.transform(sub_df['title'].fillna(''))
        texts = self.cos_sim_vectorizer.transform(sub_df['text'].fillna(''))

        sim_titles = cosine_similarity(claims[df.name], titles)[0]
        sim_texts = cosine_similarity(claims[df.name], texts)[0]

        return (sim_titles + sim_texts)/2

    def get_use_embeddings(self, X):
        input_tensor_X = tf.placeholder(tf.string, shape=(None))
        encoding_tensor_X = self.use_embed(input_tensor_X)
        with tf.Session() as session:
            session.run(tf.global_variables_initializer())
            session.run(tf.tables_initializer())

            return session.run(encoding_tensor_X, feed_dict={input_tensor_X: X})

    def get_X(self, df_orig, df_articles_true):
        df = df_orig.copy()

        df['media_labels'] = df.legal_page_urls.apply(
            lambda x: [get_label(url, self.media_labels) for url in x])
        df['known_urls'] = df.legal_page_urls.apply(
            lambda x: [url for url in x if get_label(url, self.media_labels) != 'UNKNOWN'])

        df['true_perc'] = df.media_labels.apply(
            lambda x: get_perc_from_known(x, 'TRUE'))
        df['false_perc'] = df.media_labels.apply(
            lambda x: get_perc_from_known(x, 'FALSE'))
        df['mixed_perc'] = df.media_labels.apply(
            lambda x: get_perc_from_known(x, 'MIXED'))

        df['known_count'] = df.media_labels.apply(
            lambda x: len(x) - x.count('UNKNOWN') - x.count('INVALID'))
        df['total_count'] = df.media_labels.apply(lambda x: len(x))

        df['media_titles_true'] = df.legal_page_urls.map(
            lambda x: ' '.join([get_title(y, 'TRUE', df_articles_true) for y in x]).strip())
        df['media_titles_false'] = df.legal_page_urls.map(
            lambda x: ' '.join([get_title(y, 'FALSE', df_articles_true) for y in x]).strip())
        df['media_titles_mixed'] = df.legal_page_urls.map(
            lambda x: ' '.join([get_title(y, 'MIXED', df_articles_true) for y in x]).strip())

        dfr_clf = classifier_DFR.DFR()
        df['url_categories_dfr'] = df.legal_page_urls.apply(
            lambda x: list(mit.flatten([dfr_clf.classify(y) for y in x])))
        df['domains'] = df.legal_page_urls.apply(
            lambda x: [urlparse(y).netloc.replace('www.', '') for y in x])

        df['known_perc'] = df.known_count / df.total_count
        df['known_perc'].fillna(0, inplace=True)

        claims = self.cos_sim_vectorizer.transform(df['claim'])

        df = df.reset_index(drop=True)
        df['cos_sim'] = df.apply(lambda x: self.get_cos_similarities(
            x, claims, df_articles_true), axis=1)
        df['cos_sim_avg_b'] = df['cos_sim'].apply(
            lambda x: sum(x)/(len(x) + 1))

        df['use_sim_avg_b'] = df['use_sim'].apply(
            lambda x: sum(x)/(len(x) + 1))

        #df['use_sim_avg_b'] = 0

        return df

    def predict(self, df_test):
        print('Downloading images...')
        #downloader = ImgDownloader()
        #downloader.download(df_test, 'data/raw/test/images/')

        print('Doing reverse image search...')
        with Pool(cpu_count()) as p:
            max_results = 2
            fn = partial(result_to_features, max_results=max_results)
            infos = list(tqdm.tqdm(p.imap(fn,
                                          df_test.iterrows()), total=len(df_test)))
        df_ris = pd.DataFrame(infos)
        all_urls = list(mit.flatten(df_ris.legal_page_urls))
        valid_urls = [x for x in all_urls if validators.url(x)]
        with Pool(cpu_count()) as p:
            articles = list(tqdm.tqdm(p.imap(crawl_article,
                                             valid_urls), total=len(valid_urls)))
        df_articles = pd.DataFrame(articles)
        if not df_articles.empty:
            df_articles = df_articles[df_articles.url.notna()]
            df_articles['clean_url'] = df_articles['url'].apply(clean_url)
            df_articles['media_label'] = df_articles['clean_url'].map(
                self.set_label)
            df_articles = df_articles[df_articles.media_label != ''].set_index(
                'url')

            print(df_articles.shape)

            df_articles_true = df_articles[df_articles.media_label == 'TRUE']
            df_articles_true['i'] = range(len(df_articles_true))
            print(df_articles_true.shape)
        else:
            df_articles_true = pd.DataFrame()

        claim_embeddings = self.get_use_embeddings(df_test.claim)
        title_embeddings = []
        if len(df_articles_true) > 0:
            title_embeddings = self.get_use_embeddings(
                df_articles_true.title.fillna(''))

        df_ris['use_sim'] = df_ris.apply(lambda x: get_use_similarities(
            x, claim_embeddings, title_embeddings, df_articles_true), axis=1)


        X_test = self.get_X(df_ris, df_articles_true)

        pred_proba = self.model.predict_proba(X_test)[:, 1]

        df_ris['pred_proba'] = pred_proba

        return df_ris
