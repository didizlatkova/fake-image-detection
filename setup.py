from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.0.1',
    description='Fake image detection.',
    author='Dimitrina Zlatkova',
    license='MIT',
)
